const { names, users } = require('../src/util.js');

module.exports = (app) => {
    app.get('/', (req, res) => {
        return res.send({'data': {} });
    });

    app.get('/people', (req, res) => {
        return res.send({
            people: names
        });
    })

    app.post('/person', (req, res) => {
        if(!req.body.hasOwnProperty('name')){
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter NAME'
            })
        }
        if(typeof req.body.name !== 'string'){
            return res.status(400).send({
                'error': 'Bad Request - NAME has to be a string'
            })
        }
        if(!req.body.hasOwnProperty('age')){
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter AGE'
            })
        }
        if(typeof req.body.age !== "number"){
            return res.status(400).send({
                'error': 'Bad Request - AGE has to be a number'
            })	
        }

        // Add a another if statement that returns and sends 400 status and error message if the request body does not have a username field.
         if(!req.body.hasOwnProperty('username')){
            return res.status(400).send({
                'error' : 'Bad Request: Alias is missing property'
            })
        }
    })

    // S4 Activity Template START
    app.post('/login',(req,res)=>{

        let foundUser = users.find((user) => {

            return user.username === req.body.username && user.password === req.body.password

        });

        if(!req.body.hasOwnProperty('username')){
            return res.status(400).send({
                'error' : 'Bad Request: USERNAME is missing property'
            })
        }

        if(!req.body.hasOwnProperty('password')){
            return res.status(400).send({
                'error' : 'Bad Request: PASSWORD is missing property'
            })
        }

        if(foundUser){
            return res.status(200).send({
                'success' : 'Request Granted: Login Successful'
            })
        }

        // Stretch goal
        if(!foundUser){
            return res.status(403).send({
                'error' : 'User Not Found: Login Fail'
            })
        }
    })
    // S4 Activity Template END
}