function factorial(n) {
  if (isNaN(n)) return undefined;
  if (n < 0) return undefined;
  if (n === 1 || n === 0) return 1;
  return n * factorial(n - 1);
}
function div_check(n) {
  return n % 7 === 0 || n % 5 === 0;
}

const names = {
  Brandon: {
    name: "Lebron James",
    age: 38,
  },
  Steve: {
    name: "Steve Harvey",
    age: 22,
  },
};
const users = [
  {
    username: "brBoyd87",
    password: "87brandon19",
  },
  {
    username: "tylerofsteve",
    password: "stevenstyle75",
  },
];
module.exports = {
  factorial,
  div_check,
  names,
  users,
};
